**IM3 (Intelligent multi-mode machina)** is an Arduino project to build a full personnel robot, this robot has multiple modes, like driving, autonomes...



# Modes:

<img src="media/IMG_4817.jpg" alt="IM3" width="600" align="right" style="v-index:99999"/>

1. Sensors Mode:

     distance, temp, humidity, light ...

2. Driving mode:

     control the cas by IR

3. Autonomous Mode:

     Avoid obstacles 

4. Surveillance Mode (not yet):

     detect movement and alert until IR stop

5. Talking Mode (not yet):

     speech recognition and speak

6. Merged mode (not yet):

# Todo

<img src="schema/IM3_bb.png" alt="IM3" width="600" align="right"/>

- Modes Switch
- Surveillance Mode
- Temperature and humidity
 - Make them as services
- Passive puzzer
- Talking Mode
- Merged mode


<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
