#include <AFMotor.h>

#define topSpeed 150
#define backSpeed 200
#define turnSpeed 200

bool motorsEnabled = false;
AF_DCMotor frontRightMotor(4); // create motor #2, 64KHz pwm
AF_DCMotor frontLeftMotor(1); // create motor #2, 64KHz pwm
AF_DCMotor backRightMotor(3); // create motor #2, 64KHz pwm
AF_DCMotor backLeftMotor(2); // create motor #2, 64KHz pwm

//functions
void initMotors() {
  enableMotor();
}

void setRobotSpeed(int speed) {
  if (motorsEnabled) {
    frontRightMotor.setSpeed(speed);
    frontLeftMotor.setSpeed(speed);
    backLeftMotor.setSpeed(speed);
    backRightMotor.setSpeed(speed);
  } else {
    frontRightMotor.setSpeed(0);
    frontLeftMotor.setSpeed(0);
    backLeftMotor.setSpeed(0);
    backRightMotor.setSpeed(0);
  }
}

void enableMotor() {
  motorsEnabled = true;
}

void disableMotor() {
  motorsEnabled = false;
  stopMotors(0);
}

/*
   duration = 0
   dont't use delay
*/
void moveForward(int duration) {
  Serial.println("moveForward: " + String(duration) + "ms");
  setRobotSpeed(topSpeed);
  frontRightMotor.run(FORWARD);
  frontLeftMotor.run(FORWARD);
  backLeftMotor.run(FORWARD);
  backRightMotor.run(FORWARD);
  if (duration > 0) {
    delay(duration);
    stopMotors(0);
  }
}

void moveBackward(int duration) {
  Serial.println("moveBackward: " + String(duration) + "ms");
  setRobotSpeed(backSpeed);
  frontRightMotor.run(BACKWARD);
  frontLeftMotor.run(BACKWARD);
  backLeftMotor.run(BACKWARD);
  backRightMotor.run(BACKWARD);
  if (duration > 0) {
    delay(duration);
    stopMotors(0);
  }
}

void stopMotors(int duration) {
  Serial.println("stopMotors: " + String(duration) + "ms");
  frontRightMotor.run(RELEASE);
  frontLeftMotor.run(RELEASE);
  backLeftMotor.run(RELEASE);
  backRightMotor.run(RELEASE);
  if (duration > 0) {
    delay(duration);
  }
}

void hardStopMotors() {
  Serial.println("hardStopMotors: ");
  frontRightMotor.run(BACKWARD);
  frontLeftMotor.run(BACKWARD);
  backLeftMotor.run(BACKWARD);
  backRightMotor.run(BACKWARD);
  delay(1);
  frontRightMotor.run(RELEASE);
  frontLeftMotor.run(RELEASE);
  backLeftMotor.run(RELEASE);
  backRightMotor.run(RELEASE);
}

void turnForwordRight(int duration) {
  Serial.println("turnForwordRight: " + String(duration) + "ms");
  setRobotSpeed(turnSpeed);

  frontRightMotor.run(RELEASE);
  backRightMotor.run(RELEASE);
  frontLeftMotor.run(FORWARD);
  backLeftMotor.run(FORWARD);
  delay(duration);
  stopMotors(0);
}

void turnForwordLeft(int duration) {
  Serial.println("turnForwordLeft: " + String(duration) + "ms");

  setRobotSpeed(turnSpeed);
  frontRightMotor.run(FORWARD);
  backRightMotor.run(FORWARD);
  frontLeftMotor.run(RELEASE);
  backLeftMotor.run(RELEASE);
  delay(duration);
  stopMotors(0);
}

void turnBackwordRight(int duration) {
  Serial.println("turnBackwordRight: " + String(duration) + "ms");
  frontRightMotor.run(RELEASE);
  frontLeftMotor.run(BACKWARD);
  backLeftMotor.run(BACKWARD);
  backRightMotor.run(RELEASE);
  if (duration > 0) {
    delay(duration);
    stopMotors(0);
  }
}

void turnBackwordLeft(int duration) {
  Serial.println("turnBackwordLeft: " + String(duration) + "ms");

  frontRightMotor.run(BACKWARD);
  frontLeftMotor.run(RELEASE);
  backLeftMotor.run(RELEASE);
  backRightMotor.run(BACKWARD);
  if (duration > 0) {
    delay(duration);
    stopMotors(0);
  }
}

void demoMotors() {
  Serial.println("Forword");
  moveForward(1000);
  Serial.println("BACKWARD");
  moveBackward(1000);
  Serial.println("stopMotors");
  stopMotors(1000);
  Serial.println("turnForwordRight");
  turnForwordRight(2000);
  stopMotors(1000);
  Serial.println("turnForwordLeft");
  turnForwordLeft(2000);
  Serial.println("stopMotors");
  stopMotors(1000);
}




