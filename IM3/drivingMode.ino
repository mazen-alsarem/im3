#define manualSpeed 200

void drivingOn() {
  enableMotor();
}

void drivingOff() {
  disableMotor();
}

int manualStepDuration = 1000;

void manualStepFowrord() {
  showStringOnScreen("Go", "FW");
  setRobotSpeed(manualSpeed);
  moveForward(manualStepDuration);
}

void manualStepBackword() {
  showStringOnScreen("Go", "BW");
  setRobotSpeed(manualSpeed);
  moveBackward(manualStepDuration);
}

void manualTurnForwordRight() {
  showStringOnScreen("Go", "TR");
  setRobotSpeed(manualSpeed);
  turnForwordRight(manualStepDuration);
}

void manualTurnForwordLeft() {
  showStringOnScreen("Go", "TL");
  setRobotSpeed(manualSpeed);
  turnForwordLeft(manualStepDuration);
}
