#include "IRremote.h"

// Keys
#define volUpKey 0x511DBB
#define volDownKey 0xA3C8EDDB
#define onOffKey 0xE318261B
#define nextKey 0x20FE4DBB
#define lastKey 0x52A3D41F
#define playKey 0xD7E84B1B
#define EQKey 0x97483BFB
#define key0 0xC101E57B
#define key1 0x9716BE3F
#define key2 0x3D9AE3F7
#define key3 0x6182021B
#define key4 0x8C22657B
#define key5 0x488F3CBB
#define key6 0x449E79F
#define key7 0x32C6FDF7
#define key8 0x1BC0157B
#define key9 0x3EC3FC1B
#define functionKey 0xEE886D7F
#define stKey 0xF0C41643
#define upKey 0xE5CFBD7F
#define downKey 0xF076C13B

int RECV_PIN = 10;

IRrecv irrecv(RECV_PIN);
decode_results results;

void initIrRemote() {
  irrecv.enableIRIn(); // Start the receiver
}

int IrRecivedSignal(int robotMode) {
  if (irrecv.decode(&results)) {
    Serial.println(results.value, HEX);
    switch (results.value)
    {
      case volUpKey: // VOL+ button pressed
      case 0xFF629D:
        volumeUpPressed(robotMode);
        break;
      case volDownKey: // VOL- button pressed
      case 0xFFA857:
        volumeDownPressed(robotMode);
        break;
      case onOffKey:
      case 0xFFA25D:
        onOffKeyPressed(robotMode);
        break;
      case playKey:
      case 0xFF02FD:
        playKeyPressed(robotMode);
        break;
      case nextKey:
      case 0xFFC23D:
        nextKeyPressed(robotMode);
        break;
      case lastKey:
      case 0xFF22DD:
        lastKeyPressed(robotMode);
        break;
      case EQKey:
      case 0xFF9867:
        EQKeyPressed(robotMode);
        break;
      case key0:
      case 0xFF6897:
        key0Pressed(robotMode);
        break;
      case key1:
      case 0xFF30CF:
        key1Pressed(robotMode);
        break;
      case key2:
      case 0xFF18E7:
        key2Pressed(robotMode);
        break;
      case key3:
      case 0xFF7A85:
        key3Pressed(robotMode);
        break;
      case key4:
      case 0xFF10EF:
        key4Pressed(robotMode);
        break;
      case key5:
      case 0xFF38C7:
        key5Pressed(robotMode);
        break;
      case key6:
      case 0xFF5AA5:
        key6Pressed(robotMode);
        break;
      case key7:
      case 0xFF42BD:
        key7Pressed(robotMode);
        break;
      case key8:
      case 0xFF4AB5:
        key8Pressed(robotMode);
        break;
      case key9:
      case 0xFF52AD:
        key9Pressed(robotMode);
        break;
      case functionKey:
      case 0xFFE21D:
        functionKeyPressed(robotMode);
        break;
      case stKey:
      case 0xFFB04F:
        stKeyPressed(robotMode);
        break;
      case upKey:
      case 0xFF906F:
        upKeyPressed(robotMode);
        break;
      case downKey:
      case 0xFFE01F:
        downKeyPressed(robotMode);
        break;
      case 0xFFFFFFFF:
        keyRepated(robotMode);
        break;
      default:
        anyKeyPressed(robotMode);
    }
    irrecv.resume();
  }
  delay(100);
}

