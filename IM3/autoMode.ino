
#define goTime 50
#define obstacleAlertDistance 35 // cm
#define deg2turnTime 25

void go() {
  int distance = getSonar();
  digitalPrintNumber(distance);
  Serial.println("getSonar " + String(distance) + " cm");
  if (distance <= obstacleAlertDistance) {
    hardStopMotors();
    activeBeepByDistance(distance, obstacleAlertDistance);
    Serial.println("Ops Obstacle... Search Where To Go");
    printTwoLines("Ops Obstacle", "Search Where To Go");
    int whereToGo = searchWhereToGo(); // degree where to go
    Serial.println("Found  whereToGo: " + String(whereToGo) + "deg");
    int turnDeg = abs(whereToGo) * deg2turnTime; // adapt the turn degree to the eyes degree
    printTwoLines("OK escape found", "Go to" + String(whereToGo));
    Serial.println("OK escape found: " + String(turnDeg) + "deg");
    //moveBackward(700);
    if (whereToGo < 0) {
      turnBackwordRight(700);
      turnForwordLeft(turnDeg);
    } else {
      turnBackwordLeft(700);
      turnForwordRight(turnDeg);
    }
  }
  else {
    printTwoLines(String(robotName) + " moving", "Going Forward");
    Serial.println("Going Forward " + String(goTime) + " ms");
    moveForward(0);
    delay(goTime);
  }
}

