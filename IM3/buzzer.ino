#include "pitches.h"
#include <math.h>

#define buzzerPin 34
#define activeBuzzerPin 36

bool buzzerEnabled = false;

// notes in the melody:
int melody[] = {
  NOTE_C4, NOTE_G3, NOTE_G3, NOTE_A3, NOTE_G3, 0, NOTE_B3, NOTE_C4
};

// note durations: 4 = quarter note, 8 = eighth note, etc.:
int noteDurations[] = {
  4, 8, 8, 4, 4, 4, 4, 4
};

void initBuzzer() {
  buzzerEnabled = true;
  // iterate over the notes of the melody:
  for (int thisNote = 0; thisNote < 8; thisNote++) {
    beep(thisNote, 1);
  }

}

void activeBeep(int duration) {
  if (buzzerEnabled) {
    Serial.println("activeBeep: duration: " + String(duration) + "ms");
    analogWrite(activeBuzzerPin,255);
    delay(duration);//wait for 1ms
    analogWrite(activeBuzzerPin,0);
  }
}

void beep(int type, int duration) {
  if (buzzerEnabled) {
    Serial.println("Beep: type: " + String(type) + "--> duration: " + String(duration) + "ms");

    // to calculate the note duration, take one second
    // divided by the note type.
    //e.g. quarter note = 1000 / 4, eighth note = 1000/8, etc.
    int noteDuration = 1000 / noteDurations[type];
    // to distinguish the notes, set a minimum time between them.
    // the note's duration + 30% seems to work well:
    int pauseBetweenNotes = noteDuration * 1.0;
    /*tone(buzzerPin, melody[type], noteDuration * duration);
    delay(pauseBetweenNotes);
    noTone(buzzerPin);*/
  }
}

void beepByDistance(int distance, int maxDistance) {
  int maxDuration = 20;
  int absDistance = abs(maxDistance - distance);
  int type = round(absDistance * (sizeof(melody) - 1) / maxDistance);
  int duration = round(absDistance * maxDuration / maxDistance);
  beep(type, duration);
}


void activeBeepByDistance(int distance, int maxDistance) {
  int maxDuration = 700;
  int absDistance = abs(maxDistance - distance);
  int duration = round(absDistance * maxDuration / maxDistance);
  activeBeep(duration);
}

