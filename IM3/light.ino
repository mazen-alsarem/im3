#define BLUE 34 // not yet
#define GREEN 35// not yet
#define RED 36// not yet
#define lightPinInput A15
#define lightSensible 35
#define fullLightPin 35
#define fadingDelayTime 1000 // fading time between colors

int minLight = 35;
bool enableLights = true;
bool fullLightAreOn = false;
bool manualLight = false;

void initLight() {
  /*pinMode(RED, OUTPUT);
    pinMode(GREEN, OUTPUT);
    pinMode(BLUE, OUTPUT);*/
  int maxLight =  readLight();
  minLight = maxLight * 70 / 100;
  Serial.println("maxLight: " + String(maxLight) + ": minLight " + String(minLight) );
  pinMode(fullLightPin, OUTPUT);
}

int readLight() {
  int reading  = analogRead(lightPinInput);
  int lightLevel = reading * 100 / lightSensible;
  //Serial.println("Light reading: " + String(reading) + ": level " + String(lightLevel) );
  return lightLevel;
}

void lightOff() {
  digitalWrite(RED, LOW);
  digitalWrite(GREEN, LOW);
  digitalWrite(BLUE, LOW);
}

void lightWhite() {
  digitalWrite(RED, HIGH);
  digitalWrite(GREEN, HIGH);
  digitalWrite(BLUE, HIGH);
}

void lightRed() {
  digitalWrite(RED, HIGH);
  digitalWrite(GREEN, LOW);
  digitalWrite(BLUE, LOW);
}
void lightBlue() {
  digitalWrite(RED, LOW);
  digitalWrite(GREEN, LOW);
  digitalWrite(BLUE, HIGH);
}
void lightGreen() {
  digitalWrite(RED, LOW);
  digitalWrite(GREEN, HIGH);
  digitalWrite(BLUE, LOW);
}

void lightViolet() {
  digitalWrite(RED, HIGH);
  digitalWrite(GREEN, LOW);
  digitalWrite(BLUE, HIGH);
}

void lightFadeBlue() {
  digitalWrite(RED, LOW);
  digitalWrite(GREEN, HIGH);
  digitalWrite(BLUE, HIGH);
}

void lightFadeGreen() {
  digitalWrite(RED, HIGH);
  digitalWrite(GREEN, HIGH);
  digitalWrite(BLUE, LOW);
}

void fullLightOn() {
  if (enableLights) {
    if (!fullLightAreOn) {
      Serial.println("fullLightOn" );
      fullLightAreOn = true;
    }

    analogWrite(fullLightPin, 255);
  }
}

void fullLightOff() {
  if (fullLightAreOn) {
    Serial.println("fullLightOff" );
    fullLightAreOn = false;
  }
  analogWrite(fullLightPin, 0);
}

void turnLights(bool onOff) {
  manualLight = onOff;
}

void switchLights() {
  manualLight = !manualLight;
}

void automatiqueLight() {
  int lightLevel = readLight();
  if (lightLevel < minLight || manualLight) {
    fullLightOn();
  }
  else {
    fullLightOff();
  }
}

void demoLight() {
  lightRed();
  delay(fadingDelayTime);
  lightBlue();
  delay(fadingDelayTime);
  lightGreen();
  delay(fadingDelayTime);
  lightWhite();
  delay(fadingDelayTime);
  lightViolet();
  delay(fadingDelayTime);
  lightFadeBlue();
  delay(fadingDelayTime);
  lightFadeGreen();
  delay(fadingDelayTime);
}
