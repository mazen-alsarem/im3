#include "SevSeg.h"
#include <Scheduler.h>

#define minScreenDuration 200

/*#define distanceAsPriority 1
  #define tempretureAsPriority 2
  #define commandsAsPriority 3

  int currentPriority = distanceAsPriority;*/

SevSeg sevseg;

int pinA = 22;
int pinB = 23;
int pinC = 24;
int pinD = 25;
int pinE = 26;
int pinF = 27;
int pinG = 28;
int digits[] = {32, 31, 30, 29};
int dotpin = 33;

float screenNumber = 0;
char* screenString = "";

template<int i> void screenSetup() {}
template<int i> void ScreenLoop()
{
  if (screenNumber != 0) {
    showNumber(screenNumber, 1);
  }
  if (screenString != "") {
    showString(screenString);
  }

  yield();
}

void showNumber(float num, int dot) {
  sevseg.setNumber(num, dot);
  sevseg.refreshDisplay();
}

void showString(char* txt) {
  sevseg.setChars(txt);
  sevseg.refreshDisplay();
}

void digitalPrint(float num, char* txt) {
  screenNumber = num;
  screenString = txt;
}

void digitalPrintNumber(float num) {
  Serial.println("digitalPrintNumber: " + String(num) );
  screenNumber = num;
  screenString = "";
}

void digitalPrintString(char* txt) {
  Serial.println("digitalPrintString: " + String(txt) );
  screenNumber = 0;
  screenString = txt;
}

void initDigitalScreen() {
  byte numDigits = 4;
  byte digitPins[] = {29, 30, 31, 32};
  byte segmentPins[] = {22, 23, 24, 25, 26, 27, 28, 33};
  bool resistorsOnSegments = false;
  bool updateWithDelaysIn = false;
  byte hardwareConfig = COMMON_CATHODE;
  sevseg.begin(hardwareConfig, numDigits, digitPins, segmentPins, resistorsOnSegments);
  sevseg.setBrightness(20);
  Scheduler.start(screenSetup<0>, ScreenLoop<0>);
}

/*
  void setScreenPriority(int prio){
  currentPriority = prio;
  Serial.println("setScreenPriority: " + String(prio) );
  }

  void setDistanceAsPrio(){
  setScreenPriority(distanceAsPriority);
  }
  void setTempretureAsPrio(){
  setScreenPriority(tempretureAsPriority);
  }
  void setCommandsAsPrio(){
  setScreenPriority(commandsAsPriority);
  }

  void showOnScreen(long distance, float tempreture, char* command){
   switch (currentPriority)
    {
      case distanceAsPriority:
        digitalPrintNumber(distance);
        break;
     case tempretureAsPriority:
        digitalPrintNumber(distance);
        break;
     case commandsAsPriority:
        digitalPrintString(command);
        break;
    }
  }
*/
void showLongOnScreen(char* name, long value) {
  Serial.println("showOnScreen(Long): " + String(name) + " -> " + String(value));
  digitalPrintNumber(value);
  delay(minScreenDuration);
}

void showFloatOnScreen(char* name, float value) {
  Serial.println("showOnScreen(float): " + String(name) + " -> " + String(value) );
  digitalPrintNumber(value);
  delay(minScreenDuration);
}

void showStringOnScreen(char* name, char* value) {
  Serial.println("showOnScreen(String): " + String(name) + " -> " + String(value) );
  digitalPrintString(value);
  delay(minScreenDuration);
}

void demoDigitalScreen() {
  digitalPrintString("HELO");
  delay(2000);
  String t = (String(10) + ".C");
  digitalPrintString(t.c_str());
  delay(2000);
  for (int i = 0; i < 999; i++) {
    digitalPrintNumber(i / 7);
    delay(10);
  }
  digitalPrintString("FIN");
}
