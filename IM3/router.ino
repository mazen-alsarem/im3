void anyKeyPressed(int robotMode) {
  showStringOnScreen("Key", "ANY");
}

void onOffKeyPressed(int robotMode) {
  showStringOnScreen("Key", "ON");
  if (getRobotMode() == DRIVING_MODE) {
    setRobotMode(SENSORS_MODE);
    drivingOff();
  } else {
    setRobotMode(DRIVING_MODE);
    drivingOn();
  }
}

void playKeyPressed(int robotMode) {
  showStringOnScreen("Key", "PLAY");
}
void nextKeyPressed(int robotMode) {
  showStringOnScreen("Key", "RIGHT");
  if (getRobotMode() == DRIVING_MODE) {
    manualTurnForwordRight();
  }
}

void lastKeyPressed(int robotMode) {
  showStringOnScreen("Key", "LEFT");
  if (getRobotMode() == DRIVING_MODE) {
    manualTurnForwordLeft();
  }
}

void volumeUpPressed(int robotMode) {
  showStringOnScreen("Key", "VolU");
  if (getRobotMode() == DRIVING_MODE) {
    manualStepFowrord();
  }
}

void volumeDownPressed(int robotMode) {
  showStringOnScreen("Key", "VolD");
  if (getRobotMode() == DRIVING_MODE) {
    manualStepBackword();
  }
}

void EQKeyPressed(int robotMode) {
  showStringOnScreen("Key", "EQ");

}
void key0Pressed(int robotMode) {
  showStringOnScreen("Key", "KEY0");
  setRobotMode(SENSORS_MODE);
}
void key1Pressed(int robotMode) {
  showStringOnScreen("Key", "KEY1");
  setRobotMode(DRIVING_MODE);
}
void key2Pressed(int robotMode) {
  showStringOnScreen("Key", "KEY2");
  setRobotMode(AUTO_MODE);
}
void key3Pressed(int robotMode) {
  showStringOnScreen("Key", "KEY3");
  setRobotMode(ALARM_MODE);
}
void key4Pressed(int robotMode) {
  Serial.println("key4Pressed");
}
void key5Pressed(int robotMode) {
  Serial.println("key5Pressed");
}
void key6Pressed(int robotMode) {
  Serial.println("key6Pressed");
}
void key7Pressed(int robotMode) {
  Serial.println("key7Pressed");
  long distance = getSonar();
  showLongOnScreen("distance", distance);
  delay(200);
  setDistanceAsSensor();
}
void key8Pressed(int robotMode) {
  showStringOnScreen("Key", "KEY8");
  setTempratureAsSensor();
}
void key9Pressed(int robotMode) {
  showStringOnScreen("Key", "KEY9");
  setHumidityAsSensor();
}
void functionKeyPressed(int robotMode) {
  showStringOnScreen("Key", "Func");
  switchLights();
}
void stKeyPressed(int robotMode) {
  showStringOnScreen("Key", "ST");
  setAllAsSensor();
}
void upKeyPressed(int robotMode) {
  Serial.println("upKeyPressed");
}
void  downKeyPressed(int robotMode) {
  Serial.println("downKeyPressed");
}
void  keyRepated(int robotMode) {
  Serial.println(" REPEAT");
}
