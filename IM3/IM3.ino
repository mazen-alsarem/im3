#define robotName "IM3"
#define SENSORS_MODE 0
#define DRIVING_MODE 1
#define AUTO_MODE 2
#define ALARM_MODE 3

// used output pins
/// lcd 22, 23, 24, 25, 26, 27
/// Eyes 45, 46//pmw (move 9)
/// moving 3,4,5,6,7,8,11,12/pmw
/// Active Buzzer 30
/// passive Buzzer 44 //pmw
/// temprature 1
/// telecommande 41

int robotMode = SENSORS_MODE;

int getRobotMode() {
  return robotMode;
}
void setRobotMode(int rm) {
  robotMode = rm;
}
void setup() {
  Serial.begin(9600);
  Serial.println("Welcome from " + String(robotName));
  initBuzzer();
  activeBeep(1000);
  initMotors();
  //disableMotor();
  initEyes();
  initDigitalScreen();
  initIrRemote();
  initLight();
  initTempreture();
}

void loop() {
  IrRecivedSignal(robotMode);
  switch (robotMode)
  {
    case SENSORS_MODE:
      sensorsMode();
      break;
    case DRIVING_MODE:
      break;
    case AUTO_MODE:
      go();
      break;
    case ALARM_MODE:
      break;
  }
  anyMode();
}

