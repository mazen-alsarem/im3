#include "SR04.h"
#include <Servo.h>

#define TRIG_PIN 45
#define ECHO_PIN 46
#define EYES_MOTOR 9

#define search_speed 50
#define predict_speed 50
#define search_angle_speed 10
#define predict_angle_speed 10
#define return_speed 1
//Deg 80 to 150
#define center_deg 80
#define searchMaxDegLeft 150
//Deg 0 to 80
#define searchMaxDegRight 0

SR04 sr04 = SR04(ECHO_PIN, TRIG_PIN);
Servo myServo;  // create servo object to control a servo

static int eyesPos = 0;

void initEyes() {
  myServo.attach(EYES_MOTOR);
  myServo.write(80);
}

long see() {
  long a = sr04.Distance();
  return a;
}

int predict_deg_left = 110;
int predict_deg_right = 50;

long getSonar() {
  int minDistance = 1000;
  for (int deg = center_deg; deg <= predict_deg_left; deg += predict_angle_speed) {
    myServo.write(deg);
    delay(predict_speed);
    int distance = see();
    if (distance < minDistance) {
      minDistance = distance;
    }
  }
  myServo.write(center_deg);
  for (int deg = center_deg; deg > predict_deg_right; deg -= predict_angle_speed) {
    myServo.write(deg);
    delay(predict_speed);
    int distance = see();
    if (distance < minDistance) {
      minDistance = distance;
    }
  }

  return minDistance;
}

String distansAsString() {
  long a = sr04.Distance();
  return String(a);
}

/**
   return -deg to say go left for 'deg'
   return +deg to say go right for 'deg'
*/
int searchWhereToGo() {
  int* searchResultLeft = searchLeft();
  int* searchResultRight = searchRight();
  if (searchResultLeft[0] > searchResultRight[0]) { // max distance found at left
    return center_deg - searchResultLeft[1]; // return the degree where we found it
  } else {
    return center_deg - searchResultRight[1]; // return the degree where we found it
  }
}


int* searchLeft() {
  int searchResult[] = {
    0,// int max_dist;
    0,// int max_dist_dig;
    1000,// int min_dist;
    360,// int min_dist_dig;
  };

  for (int deg = center_deg; deg <= searchMaxDegLeft; deg += search_angle_speed) {
    Serial.println("Move left to " + String(deg) + "deg");
    myServo.write(deg);
    delay(search_speed);
    int distance = see();
    digitalPrintNumber(distance);
    //Serial.println("Dis: " + String(searchResult[2]) + "--> " + String(searchResult[0]) + "cm");
    //Serial.println("Deg " + String(searchResult[3]) + " --> " + String(searchResult[1]) + "deg");
    //Serial.println("dis = " + String(distance));
    beepByDistance(distance, searchResult[0] );
    if (searchResult[0] < distance) {
      searchResult[0] = distance;
      searchResult[1] = deg;
    }
    if (searchResult[2] > distance) {
      searchResult[2] = distance;
      searchResult[3] = deg;
    }
  }

  for (int deg = searchMaxDegLeft; deg >= center_deg ; deg -= search_angle_speed) {
    myServo.write(deg);
    delay(return_speed);
  }
  return searchResult;
}

int* searchRight() {
  Serial.println("searchRight");
  int searchResult[] = {
    0,// int max_dist;
    0,// int max_dist_dig;
    1000,// int min_dist;
    360,// int min_dist_dig;
  };

  for (int deg = center_deg; deg >= searchMaxDegRight; deg -= search_angle_speed) {
    Serial.println("Move right to " + String(deg) + "deg");
    myServo.write(deg);
    delay(search_speed);
    int distance = see();
    digitalPrintNumber(distance);
    beepByDistance(distance, searchResult[0] );
    //Serial.println("Dis: " + String(searchResult[2]) + "--> " + String(searchResult[0]) + "cm");
    //Serial.println("Deg " + String(searchResult[3]) + " --> " + String(searchResult[1]) + "deg");
    //Serial.println("dis = " + String(distance));

    if (searchResult[0] < distance) {
      searchResult[0] = distance;
      searchResult[1] = deg;
    }
    if (searchResult[2] > distance) {
      searchResult[2] = distance;
      searchResult[3] = deg;
    }
  }

  for (int deg = searchMaxDegRight; deg <= center_deg ; deg += search_angle_speed) {
    myServo.write(deg);
    delay(return_speed);
  }

  return searchResult;
}

void printSearchResult(int searchResult[]) {
  printTwoLines("Dis: " + String(searchResult[2]) + "--> " + String(searchResult[0]) + "cm", "Deg " + String(searchResult[3]) + " --> " + String(searchResult[1]) + "deg");
}

void demoEyes() {
  printTwoLines("Eyes demo: I see ", "Object at " + distansAsString() + "cm");
  Serial.println("Object at " + distansAsString() + "cm");
  delay(100);
}

void servoDemo() {
  for (int deg = 10; deg < 100; deg += 10) {
    myServo.write(deg);
    delay(500);
  }
}
