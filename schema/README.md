# Schema and Parts of **IM3 (Intelligent multi-mode machina)** 
## Parts
### Main
- Arduino Mega 2560
 - Elegoo Carte MEGA 2560 R3 ATmega2560 ATMEGA 16U2 Board Noir avec Câble USB pour Arduino
- Chassis voiture Arduino
 - TBS 2652 Chassis voiture Arduino, Smart Robot Car Arduino with Speed Encoder

### Eyes
- Servo motor
- Ultrasonic Sensor Module

### Move
- 4x DC Motors
- L293D Motor Drive Shield
 - Muzoct L293D Motor Drive Shield pour Arduino Duemilanove Mega UNO R3 AVR ATMEL

### Control
- IR Receiver Module
- IR Remote control

### Show
- Segment Display
- Four Digital Seven Segment Display 

### Sounds
- Active buzzer
- Passive Buzzer 

### Light
- LEDs
- Photocell
- Relay

### Sences
- DHT11 Temperature and Humidity Sensor

### Battaries 
- 4x AA
- 2x 18650
- 1x 9v

# Schema
![Schema](schema/IM3_bb.png)


